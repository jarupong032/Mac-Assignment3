package com.example.mac.pro3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import Model.UserModel;

public class DatabaseHelper extends SQLiteOpenHelper{
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "register";
    private static final String ID = "id";
    private static final String TABLE_USER = "user";
    private static final String COL_TITLE = "title";
    private static final String COL_DESCRIPTION = "description";

    private static final String CREATE_DATABASE =
            "CREATE TABLE " + TABLE_USER + "(" + ID + " INTEGER PRIMARY KEY, "
                    + COL_TITLE + " VARCHAR(50)," + COL_DESCRIPTION + " VARCHAR(50)" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DATABASE);
        Log.i(TAG, "CREATE DATABASE");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        Log.i(TAG, "UPGRADE DATABASE");
        onCreate(db);
    }

    public List<UserModel> readUser(){
        List<UserModel> listUser = new ArrayList<>();
        String select = "SELECT * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(select,null);
        if (cursor.moveToFirst()){
            do{
                UserModel model = new UserModel();
                model.setId(cursor.getString(cursor.getColumnIndex(ID)));
                model.setTitle(cursor.getString(cursor.getColumnIndex(COL_TITLE)));
                model.setDescription(cursor.getString(cursor.getColumnIndex(COL_DESCRIPTION)));
                listUser.add(model);
            }while (cursor.moveToNext());
        }
        db.close();
        cursor.close();

        return  listUser;
    }


    public void insertUser(String title,String description){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title",title);
        values.put("description",description);

        db.insert(TABLE_USER,null,values);
    }

    public  void  updateUser(String id,String title,String description){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title",title);
        values.put("description",description);

        db.update(TABLE_USER,values,ID + " = ?",new String[]{id});
        Log.i(TAG, "UPDATE DATABASE");
    }

    public  void deleteUser(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER,ID + " = ?",new String[]{id});
        Log.i(TAG, "DELETE DATABASE");
    }
}
