package com.example.mac.pro3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertActivity extends AppCompatActivity{
    private EditText et_title,et_description;
    private Button btn_insert;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        Bundle bundle = getIntent().getExtras();

        bindView();

        btn_insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertData();
            }
        });
        if (bundle != null){
            final String position = bundle.getString("position");
            btn_insert.setText("update");
            btn_insert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateData(position);
                }
            });
        }
    }

    private void bindView(){
        et_title = (EditText) findViewById(R.id.title);
        et_description = (EditText) findViewById(R.id.description);
        btn_insert = (Button) findViewById(R.id.btn_insert);
    }

    private void insertData(){
        String title = et_title.getText().toString();
        String description = et_description.getText().toString();
        if(!title.isEmpty()&&!description.isEmpty()){
            DatabaseHelper helper = new DatabaseHelper(this);
            helper.insertUser(title,description);
            finish();
        }else {
            Toast.makeText(this,"กรุณากรอกข้อมูลให้ครบ",Toast.LENGTH_SHORT).show();
        }
    }

    private void  updateData(String id){
        String title = et_title.getText().toString();
        String description = et_description.getText().toString();
        if(!title.isEmpty()&&!description.isEmpty()){
            DatabaseHelper helper = new DatabaseHelper(this);
            helper.updateUser(id,title,description);
            finish();
        }else {
            Toast.makeText(this,"กรุณากรอกข้อมูลให้ครบ" ,Toast.LENGTH_SHORT).show();
        }
    }
}
